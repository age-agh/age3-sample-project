# AgE 3

AgE 3 is a new version and complete rewrite of the distributed agent-based computational platform
[AgE](https://www.age.agh.edu.pl/).

**This is beta software** and is still under heavy development. Although this version is released, it is only
a *snapshot* of a current development branch and there is no guarantee that future version will provide the same
functionality or API.

This project contains a sample computational project, defining a single runnable and the Spring configuration.

For more info, check the AgE 3 repository and its documentation, especially page about running AgE:
<https://docs.age.agh.edu.pl/age3/user/running.html>.

## Links

- [AgE 3 Repository on GitLab](https://gitlab.com/age-agh/age3)
- [This project's repository on GitLab](https://gitlab.com/age-agh/age3-sample-project)
